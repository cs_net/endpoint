# EndPoint
- Endpoint is a set of user tools you can hook into a user account
- Make a .env file and set up as below

## Run in Docker
- Go into setup/ and run the following:
  - `docker compose build`
  - `docker compose run arch`
- A version of the code is stored as read-only in `/usr/local/src/endPoint`

## Set up server
- Run the script in `setup/scripts/system.sh` as root
  - `/usr/local/src/endPoint/setup/scripts/system.sh`

## Add endPoint to user account
- Copy endPoint code locally (e.g. to `$HOME/.local/endPoint`) and ensure your current user is the owner
- Run the script in `setup/scripts/user.sh` as the user
  - `mkdir -p $HOME/.local; cp -r /usr/local/src/endPoint $HOME/.local`
  - `chown -R $USER:$USER $HOME/.local/endPoint`
  - `$HOME/.local/endPoint/setup/scripts/user.sh`
  - `source $HOME/.bash_profile`
