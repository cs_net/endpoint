#!/bin/bash
[[ "$EUID" -ne 0 ]] && { echo "Run as root"; exit 1; }

# Source environment data
endPoint=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")/../..
source $endPoint/passive/_base

# Configure boot system (have to install grub separately)
if ! grep "encrypt lvm2 filesystems" /etc/mkinitcpio.conf; then
	sed -E 's/(^HOOKS.*)(filesystems)(.*)/\1encrypt lvm2 \2\3/' /etc/mkinitcpio.conf
	mkinitcpio -P
fi
grub_lock

# Download all system
sed -i 's/^# \(ParallelDownloads\)/\1/' /etc/pacman.conf
cat $endPoint/setup/packages/* | pacman -Syu --noconfirm --needed -
pacman -Scc --noconfirm

# Setup local preferences
sed -i 's/^# \(en_GB.UTF-8 UTF-8\)/\1/' /etc/locale.gen && locale-gen && locale > /etc/locale.conf
rmmod pcspkr && echo "blacklist pcspkr" >> /etc/modprobe.d/blacklist.conf
echo -e "Defaults rootpw\n%wheel ALL=(ALL:ALL) ALL\n" >> /etc/sudoers
systemctl enable NetworkManager
ln -s /usr/bin/vim /usr/bin/vi
sed -i -E 's/(^[^#].*opacity) = [0-9]+\.[0-9]+;(.*$)/\1 = 1;\2/' /etc/xdg/picom.conf

# Set up brightness script
cp $endPoint/setup/misc/brightness /usr/local/bin/brightness && \
 echo '%wheel ALL = NOPASSWD: /usr/local/bin/brightness' >> /etc/sudoers

# Install DWM and DMENU
srcDir="/usr/local/src"
mkdir -p ${sys_SRC}; rm -rf ${sys_SRC}/{dwm,dmenu}
## If these fail in compilation then config.h files are out of date
cd ${sys_SRC}
 git clone https://git.suckless.org/dwm && \
 cd dwm && \
 cp $endPoint/setup/config/dwm/config.h . && \
 make clean install
cd ${sys_SRC}
 git clone https://git.suckless.org/dmenu && \
 cd dmenu && \
 cp $endPoint/setup/config/dmenu/config.h . && \
 make clean install
cd ${sys_SRC}
 git clone https://github.com/yt-dlp/yt-dlp.git && \
 echo -e "#!/bin/bash\nbash ${sys_SRC}/yt-dlp/yt-dlp.sh $@" > /usr/local/bin/youtube-dl && \
 chmod a+x /usr/local/bin/youtube-dl

