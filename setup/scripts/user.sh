#!/bin/bash
# This sets up a user to use endPoint

# Source environment data
endPoint=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")/../..
source $endPoint/passive/_base

mkdir -p "$XDG_LOCAL_HOME" "$XDG_CONFIG_HOME" $HOME/.vim

CFG="$endPoint/config"

if [[ "$1" == "undo" ]]
then
    cd $HOME
     unlink .bashrc; unlink .bash_profile; unlink .vim/colors
    cd -

	cd $XDG_CONFIG_HOME
	 unlink alacritty; unlink mpd; unlink mpv; unlink ncmpcpp; unlink ranger
	 unlink tmux; unlink X11; unlink newsboat; unlink vimrc
	cd -

	unlink $CFG/newsboat/urls
else
	echo "Do ./user.sh undo to unlink..."

	# "Delete" these files
	[[ -f $HOME/.bashrc ]] && mv $HOME/.bashrc /tmp
	[[ -f $HOME/.bash_profile ]] && mv $HOME/.bash_profile /tmp

	ln -Ts $CFG/bash_profile $HOME/.bash_profile
	ln -Ts $HOME/.bash_profile $HOME/.bashrc

	ln -Ts $CFG/alacritty $XDG_CONFIG_HOME/alacritty
	ln -Ts $CFG/mpd $XDG_CONFIG_HOME/mpd
	ln -Ts $CFG/mpv $XDG_CONFIG_HOME/mpv
	ln -Ts $CFG/ncmpcpp $XDG_CONFIG_HOME/ncmpcpp
	ln -Ts $CFG/picom $XDG_CONFIG_HOME/picom
	ln -Ts $CFG/ranger $XDG_CONFIG_HOME/ranger
	ln -Ts $CFG/tmux $XDG_CONFIG_HOME/tmux
	ln -Ts $CFG/X11 $XDG_CONFIG_HOME/X11
	ln -Ts $CFG/zathura $XDG_CONFIG_HOME/zathura

	ln -Ts $CFG/newsboat $XDG_CONFIG_HOME/newsboat
	ln -Ts $XDG_LOCAL_HOME/newsboat_urls $CFG/newsboat/urls

	ln -Ts $CFG/vim/vimrc $XDG_CONFIG_HOME/vimrc
	ln -Ts $CFG/vim_colors $HOME/.vim/colors
fi

git config --global init.defaultBranch main
[[ -n "$GIT_NAME" ]] && git config --global user.name "$GIT_NAME"
[[ -n "$GIT_EMAIL" ]] && git config --global user.email "$GIT_EMAIL"

#ln -Ts $MUSICDIR $HOME/Music
